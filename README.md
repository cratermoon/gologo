# gologo

An experiment in language interpreters and graphics.

Given a LOGO-like program,
draw shapes based on the commands given.

## PEN DOWN | UP

lower/raise pen.
Changes the behavior of [MOVE](#move-n)

## MOVE *n*

Move the turtle *n* units.
If the [pen](#pen-down-up) is DOWN this will draw a line.
If the pen is UP it will move without creating a visible line.

## TURN θ

Change the turtle facing,
in degrees by default.
This can be changed to radians with the [UNITS](#unit-raddeg) command.

## GRID MxN

sets the size of the grid. default is 1000x1000

## UNIT RAD|DEG

Change theta angle units.
Default is degrees.

## CLEAR

Erase the [grid](#grid-mxn)

## HOME

Return to grid origin
