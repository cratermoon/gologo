package main

import (
	"flag"
	"io"
	"log"
	"os"

	"gitlab.com/cratermoon/gologo/draw"
	"gitlab.com/cratermoon/gologo/parser"
	"gitlab.com/cratermoon/gologo/tokenizer"
)

var (
	inputFile = flag.String("i", "", "input file")
	debug     = flag.Bool("d", false, "debug")
	// outputFile = flag.String("o", "", "output file")
)

func main() {
	flag.Parse()
	if *inputFile == "" {
		flag.Usage()
		os.Exit(1)
	}
	if !*debug {
		log.SetOutput(io.Discard)
	}
	in, err := os.ReadFile(*inputFile)
	if err != nil {
		log.Fatal(err)
	}

	t, tokenChan := tokenizer.Tokenize(in)
	if t == nil {
		log.Fatal("nil tokenizer")
	}
	lp, astChan := parser.Parse(tokenChan)
	if lp == nil {
		log.Fatal("nil parser")
	}
	draw.Draw(*inputFile, astChan)
	if te := t.Err(); te != nil {
		log.Printf("tokenizer error %s", te)
	}
}
