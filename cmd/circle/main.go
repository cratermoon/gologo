package main

import (
	"image"
	"image/color"
	"math"

	"github.com/llgcode/draw2d/draw2dimg"
)


func main() {
	width := 1000
	height := 1000
	destination := image.NewRGBA(image.Rect(0, 0, 1000, 1000))

	gc := draw2dimg.NewGraphicContext(destination)
	// Set some properties
	gc.SetStrokeColor(color.RGBA{0x10, 0x10, 0x18, 0xfa})
	gc.SetLineWidth(3)
	Box(gc, width, height)
	startAngle := 270 *  (math.Pi / 180.0)
	angle := 359 * (math.Pi / 180.0)  
	gc.ArcTo(500.0, 500.0, 250.0, 250.0, startAngle, angle)
	gc.Stroke()
	draw2dimg.SaveToPngFile("circle.png", destination)
}



func Box(gc *draw2dimg.GraphicContext, width, height int) {
	x := float64(width)
	y := float64(height)
	// Draw a closed shape
	gc.BeginPath()  // Initialize a new path
	gc.MoveTo(0, 0) // Move to a position to start the new path
	gc.LineTo(x, 0)
	gc.MoveTo(x, 0)
	gc.LineTo(x, y)
	gc.MoveTo(x, y)
	gc.LineTo(0, y)
	gc.MoveTo(0, y)
	gc.LineTo(0, 0)
	gc.FillStroke()
}
