package tokenizer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/cratermoon/gologo/tokenizer"
)

func TestPenCommand(t *testing.T) {
	tknizer, tokenChan := tokenizer.Tokenize([]byte("PEN DOWN"))
	tokens := make([]tokenizer.Token, 0, 2)
	for token := range tokenChan {
		tokens = append(tokens, token)
	}

	assert.Nil(t, tknizer.Err())

	require.Len(t, tokens, 2)
	assert.Equal(t, tokenizer.PEN, tokens[0].Kind())
	assert.Equal(t, "PEN", tokens[0].Value())
	assert.Equal(t, "DOWN", tokens[1].Value())
	assert.Equal(t, 1, tokens[0].LineNumber())
	assert.Equal(t, 1, tokens[1].LineNumber())
}

func TestMoveCommand(t *testing.T) {
	tknizer, tokenChan := tokenizer.Tokenize([]byte("MOVE 10"))
	tokens := make([]tokenizer.Token, 0, 2)
	for token := range tokenChan {
		tokens = append(tokens, token)
	}

	assert.Nil(t, tknizer.Err())
	require.Len(t, tokens, 2)
	assert.Equal(t, tokenizer.MOVE, tokens[0].Kind())
	assert.Equal(t, "MOVE", tokens[0].Value())
	// assert.Equal(t, tokenizer.PEN, tokens[0].Kind())
	assert.Equal(t, "10", tokens[1].Value())
}

func TestTurnCommand(t *testing.T) {
	tknizer, tokenChan := tokenizer.Tokenize([]byte("TURN 90"))
	tokens := make([]tokenizer.Token, 0, 2)
	for token := range tokenChan {
		tokens = append(tokens, token)
	}

	assert.Nil(t, tknizer.Err())
	require.Len(t, tokens, 2)
	assert.Equal(t, tokenizer.TURN, tokens[0].Kind())
	assert.Equal(t, "TURN", tokens[0].Value())
	assert.Equal(t, "90", tokens[1].Value())
}
