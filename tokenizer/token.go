package tokenizer

type TokenKind string

const (
	NUMBER    TokenKind = "number"
	ANGLE     TokenKind = "angle"
	DIRECTION TokenKind = "direction"
	PEN       TokenKind = "pen"
	MOVE      TokenKind = "move"
	TURN      TokenKind = "turn"
	GRID      TokenKind = "grid"
	UNIT      TokenKind = "unit"
	CIRCLE    TokenKind = "circle"
	CIRCLEF   TokenKind = "circlefilled"
	CLEAR     TokenKind = "clear"
	HOME      TokenKind = "home"
)

var CommandTokens = map[string]TokenKind{
	"PEN":     PEN,
	"MOVE":    MOVE,
	"TURN":    TURN,
	"CIRCLE":  CIRCLE,
	"CIRCLEF": CIRCLEF,
}

type Token interface {
	Kind() TokenKind
	Value() string
	LineNumber() int
}

type Tokenizer interface {
	Err() error
}
