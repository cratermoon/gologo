package tokenizer

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
)

type token struct {
	kind       TokenKind
	value      string
	lineNumber int
}

func (t token) Kind() TokenKind {
	return t.kind
}
func (t token) Value() string {
	return t.value
}
func (t token) LineNumber() int {
	return t.lineNumber
}
func (t token) String() string {
	return fmt.Sprintf("%s:%s @ %d", t.kind, t.value, t.lineNumber)
}

const COMMAND = "pen"

type tokenizer struct {
	input  []byte
	tokens chan Token
	err    *TokenizerError
}

func (t tokenizer) Err() error {
	return t.err
}

func (err TokenizerError) Error() string {
	return fmt.Sprintf("%s at line %d", err.error, err.lineNumber) // TODO: Implement
}

var lineNumber int

type TokenizerError struct {
	error
	lineNumber int
}

func Tokenize(input []byte) (Tokenizer, chan Token) {
	tknz := tokenizer{
		input:  input,
		tokens: make(chan Token),
		err:    nil,
	}
	go tknz.run()
	return tknz, tknz.tokens
}

func (toknizr *tokenizer) run() {
	defer close(toknizr.tokens)
	log.Println("kind,value,lineno")
	sc := bufio.NewScanner(bytes.NewBuffer(toknizr.input))
	sc.Split(bufio.ScanLines)
	lineNumber = 1
	for sc.Scan() {
		line := sc.Text()
		tokenize(toknizr.tokens, line)
		lineNumber++
	}
	if err := sc.Err(); err != nil {
		toknizr.err = &TokenizerError{
			error:      err,
			lineNumber: lineNumber,
		}
	}
}

var counter int

func advance() {
	counter++
}

func tokenize(out chan Token, text string) {
	if len(text) == 0 {
		return
	}
	log.Println("text: " + text)
	counter = 0
	for counter < len(text) {
		c := text[counter]
		switch c {
		case ' ':
			advance()
		default:
			if isAlpha(c) {
				token := identifier(text[counter:])
				out <- token
				log.Printf("%s\n", token)
			} else if isDigit(c) {
				token := number(text[counter:])
				out <- token
				log.Printf("%s\n", token)
			}
		}
	}
}

func isAlpha(c byte) bool {
	log.Printf("isAlpha c: %c", c)
	return c >= 'A' && c <= 'Z'
}

func isDigit(c byte) bool {
	log.Printf("isDigit c: %c", c)
	return c >= '0' && c <= '9'
}

func identifier(text string) token {
	i := 0
	for ; i < len(text) && isAlpha(text[i]); i++ {
		advance()
	}
	val := text[0:i]
	if CommandTokens[val] != "" {
		return token{
			kind:       CommandTokens[val],
			value:      val,
			lineNumber: lineNumber,
		}

	}
	return token{
		kind:       DIRECTION,
		value:      val,
		lineNumber: lineNumber,
	}
}

func number(text string) token {
	i := 0
	for ; i < len(text) && isDigit(text[i]); i++ {
		advance()
	}

	return token{
		kind:       NUMBER,
		value:      text[0:i],
		lineNumber: lineNumber,
	}

}
