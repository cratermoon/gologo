module gitlab.com/cratermoon/gologo

go 1.22.0

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/llgcode/draw2d v0.0.0-20240322162412-ee6987bd01dc // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	golang.org/x/image v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
