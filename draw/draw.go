package draw

import (
	"image"
	"image/color"
	"log"
	"math"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/llgcode/draw2d/draw2dimg"
	"gitlab.com/cratermoon/gologo/parser"
)

var direction float64 = 90.0 * math.Pi / 180.0

const twoPi = math.Pi * 2.0

type pen func(*draw2dimg.GraphicContext, float64, float64)

var move pen = func(gc *draw2dimg.GraphicContext, x, y float64) {
	gc.MoveTo(x, y)
}

var line pen = func(gc *draw2dimg.GraphicContext, x, y float64) {
	gc.LineTo(x, y)
}

type Turtle struct {
	penAction   pen
	direction   float64
	hx, hy      float64
	gc          *draw2dimg.GraphicContext
	destination *image.RGBA
	drawingName string
}

func Draw(drawingName string, astChan chan parser.Node) {
	t := Turtle{
		penAction:   move,
		direction:   90.0 * math.Pi / 180.0,
		drawingName: drawingName,
	}
	for ast := range astChan {
		draw(&t, ast)
		t.gc.Close()
		t.gc.Stroke()
		parser.PrettyPrint(ast)
		draw2dimg.SaveToPngFile(outputFileName(t.drawingName), t.destination)
	}
}

func draw(t *Turtle, n parser.Node) {
	switch n.Kind() {
	case parser.PROGRAM:
		t.startProgram()
		for _, nd := range n.Body() {
			draw(t, nd)
		}
	case parser.COMMAND:
		switch n.Value() {
		case "PEN":
			// up or down?
			if n.Body()[0].Value() == "DOWN" {
				t.penAction = line
			} else {
				t.penAction = move
			}
		case "TURN":
			// tricky?
			parm := n.Body()[0].Value() // need better error handling
			v, err := strconv.ParseFloat(parm, 64)
			if err != nil {
				log.Fatal(err)
			}
			direction = direction + v*math.Pi/180.0 // degrees to radians
			if direction > twoPi {
				direction = direction - twoPi
			}

		case "MOVE":
			parm := n.Body()[0].Value() // need better error handling
			distance, err := strconv.ParseFloat(parm, 64)
			if err != nil {
				log.Fatal(err)
			}
			t.hx = t.hx + distance*math.Sin(direction)
			t.hy = t.hy + -1*distance*math.Cos(direction) // origin is top left
			log.Printf("%s %f", n.Value(), distance)
			t.penAction(t.gc, t.hx, t.hy)
		case "CIRCLE":
			parm := n.Body()[0].Value() // need better error handling
			radius, err := strconv.ParseFloat(parm, 64)
			if err != nil {
				log.Fatal(err)
			}
			t.circle(n.Value(), radius)
		case "CIRCLEF":
			parm := n.Body()[0].Value() // need better error handling
			radius, err := strconv.ParseFloat(parm, 64)
			if err != nil {
				log.Fatal(err)
			}
			t.circle(n.Value(), radius)
			t.gc.SetFillColor(color.RGBA{0xFF, 0xFF, 0xFF, 0xFF})
			t.gc.FillStroke()
		default:
			for _, nd := range n.Body() {
				draw(t, nd)
			}
		}
	default:
	}
}

func (t *Turtle) circle(value string, radius float64) {
	log.Printf("%s %f", value, radius)
	// this zeros out any remaining undrawn paths
	t.gc.Stroke()
	t.gc.ArcTo(t.hx, t.hy, radius, radius, 0.0, 360.0)
}

func (t *Turtle) startProgram() {
	// Initialize the graphic context on an RGBA image
	width := 1000
	height := 1000
	t.destination = image.NewRGBA(image.Rect(0, 0, 1000, 1000))

	t.gc = draw2dimg.NewGraphicContext(t.destination)
	// Set some properties
	t.gc.SetStrokeColor(color.RGBA{0x10, 0x10, 0x18, 0xfa})
	t.gc.SetLineWidth(3)
	Box(t.gc, width, height)
}

func Box(gc *draw2dimg.GraphicContext, width, height int) {
	x := float64(width)
	y := float64(height)
	// Draw a closed shape
	gc.BeginPath()  // Initialize a new path
	gc.MoveTo(0, 0) // Move to a position to start the new path
	gc.LineTo(x, 0)
	gc.MoveTo(x, 0)
	gc.LineTo(x, y)
	gc.MoveTo(x, y)
	gc.LineTo(0, y)
	gc.MoveTo(0, y)
	gc.LineTo(0, 0)
	gc.FillStroke()
}

func outputFileName(name string) string {
	return filepath.Base(strings.TrimSuffix(name, filepath.Ext(name))) + ".png"
}
