package parser

import (
	"fmt"

	"gitlab.com/cratermoon/gologo/tokenizer"
)

type NodeKind string

const (
	PROGRAM   NodeKind = "program"
	NUMBER    NodeKind = "number"
	COMMAND   NodeKind = "command"
	DIRECTION NodeKind = "direction"
	UNKNOWN   NodeKind = "unknown"
)

type Node struct {
	kind       NodeKind
	value      string
	lineNumber int
	body       []Node
}

func (n Node) Kind() NodeKind {
	return n.kind
}

func (n Node) Value() string {
	return n.value
}

func (n Node) Body() []Node {
	return n.body
}

func (n Node) LineNumber() int {
	return n.lineNumber
}

func (n *Node) AppendChild(newNode Node) {
	n.body = append(n.body, newNode)
}

func (n Node) String() string {
	return fmt.Sprintf("%s:%s@%d", n.kind, n.value, n.lineNumber)
}

func NewNode(kind NodeKind, value string, lineNo int) Node {
	return Node{
		kind:       kind,
		value:      value,
		lineNumber: lineNo,
	}
}

func NewCommandNode(t tokenizer.Token) Node {
	return NewNode(COMMAND, t.Value(), t.LineNumber())
}

func NewNumberNode(t tokenizer.Token) Node {
	return NewNode(NUMBER, t.Value(), t.LineNumber())
}

func NewDirectionNode(t tokenizer.Token) Node {
	return NewNode(DIRECTION, t.Value(), t.LineNumber())
}

type AST Node

var ProgramNode = Node{
	kind:  PROGRAM,
	value: "program",
}

type Parser interface{}
