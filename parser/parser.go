package parser

import (
	"log"
	"strings"

	"gitlab.com/cratermoon/gologo/tokenizer"
)

type logoParser struct {
	inChan  chan tokenizer.Token
	outChan chan Node
}

func (lp logoParser) run() {
	ast := ProgramNode
	log.Print("begin Parse")
	defer close(lp.outChan)
	defer log.Print("end Parse")
	for token := range lp.inChan {
		ast.AppendChild(lp.walk(token))
	}
	lp.outChan <- ast
}

func Parse(inChan chan tokenizer.Token) (Parser, chan Node) {
	lp := logoParser{
		inChan:  inChan,
		outChan: make(chan Node),
	}
	go lp.run()
	return lp, lp.outChan
}

func (lp logoParser) walk(t tokenizer.Token) Node {
	log.Printf("walk %s", t)
	switch t.Kind() {
	case tokenizer.MOVE:
		fallthrough
	case tokenizer.PEN:
		fallthrough
	case tokenizer.CIRCLE:
		fallthrough
	case tokenizer.CIRCLEF:
		fallthrough
	case tokenizer.TURN:
		node := NewCommandNode(t)
		body := lp.walk(<-lp.inChan)
		node.AppendChild(body)
		return node
	case tokenizer.NUMBER:
		return NewNumberNode(t)
	case tokenizer.DIRECTION:
		return NewDirectionNode(t)
	default:
		return NewNode(
			UNKNOWN,
			t.Value(),
			t.LineNumber())
	}
}

var Depth = 1

func PrettyPrint(n Node) {
	var sb strings.Builder
	for i := 1; i < Depth; i++ {
		sb.WriteString("   ")
	}
	indent := sb.String()
	Depth++
	switch n.Kind() {
	case PROGRAM:
		fallthrough
	case COMMAND:
		log.Printf("%s%s [\n", indent, n)
		for _, nd := range n.Body() {
			PrettyPrint(nd)
		}
		log.Printf("%s]\n", indent)
	default:
		log.Printf("%s%s\n", indent, n)
	}
	Depth--
}
